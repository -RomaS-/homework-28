package com.stolbunov.roman.data.repository;

import com.stolbunov.roman.data.entity.LocaleNote;
import com.stolbunov.roman.data.mapper.NoteMapper;
import com.stolbunov.roman.data.repository.data_source.DBNoteRepository;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RepositoryManagerI implements INoteRepositoryBoundary {

    @Inject
    DBNoteRepository db;

    public RepositoryManagerI(DBNoteRepository db) {
        this.db = db;
    }

    @Override
    public Observable<Note> add(Note note) {
        return Observable.just(note)
                .map(NoteMapper::transform)
                .flatMap(db::add)
                .map(NoteMapper::transform)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public boolean remove(Note note) {
        LocaleNote localeNote = NoteMapper.transform(note);
        return db.remove(localeNote);
    }

    @Override
    public Observable<Note> change(Note note) {
        return Observable.just(note)
                .map(NoteMapper::transform)
                .flatMap(db::changed)
                .map(NoteMapper::transform)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<Note>> uploadNoteList() {
        return db.getNoteList()
                .map(NoteMapper::transformList)
                .subscribeOn(Schedulers.io());
    }
}
