package com.stolbunov.roman.data.repository;

import com.stolbunov.roman.data.entity.LocaleNote;

import java.util.List;

import io.reactivex.Observable;

public interface IDBNote {

    Observable add(LocaleNote note);

    boolean remove(LocaleNote note);

    Observable<LocaleNote> changed(LocaleNote note);

    Observable<List<LocaleNote>> getNoteList();
}
