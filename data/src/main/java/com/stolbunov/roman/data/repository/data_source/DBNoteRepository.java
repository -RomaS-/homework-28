package com.stolbunov.roman.data.repository.data_source;

import android.content.SharedPreferences;

import com.stolbunov.roman.data.entity.LocaleNote;
import com.stolbunov.roman.data.repository.IDBNote;
import com.stolbunov.roman.domain.exeption.RepositoryExeption;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DBNoteRepository implements IDBNote {
//    private final String NAME_DB = "notes";
    private static final String KEY_NUMBER_NOTES = "number notes";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_PRIORITY = "priority";

    private static long counter = 0;

    @Inject
    private SharedPreferences preferences;

    public DBNoteRepository(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Observable<LocaleNote> add(LocaleNote note) {
        return Observable.just(note)
                .map(note1 -> addNewNote(note));
    }

    @Override
    public boolean remove(LocaleNote note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_TITLE + note.getId());
        editor.remove(KEY_DESCRIPTION + note.getId());
        editor.remove(KEY_PRIORITY + note.getId());
        return editor.commit();
    }

    @Override
    public Observable<LocaleNote> changed(LocaleNote note) {
        return Observable.just(note)
                .map(val -> changedNote(note));
    }

    @Override
    public Observable<List<LocaleNote>> getNoteList() {
        long dbSize = preferences.getLong(KEY_NUMBER_NOTES, -1);
        return Observable.just(dbSize)
                .filter(this::emptyDBFilter)
                .map(this::uploadNoteList);
    }

    private boolean emptyDBFilter(long dbSize) {
        if (dbSize < 0) {
            counter = 0;
            return false;
        } else {
            counter = ++dbSize;
            return true;
        }
    }

    private List<LocaleNote> uploadNoteList(long numberNotes) {
        List<LocaleNote> notes = new LinkedList<>();
        for (int i = 0; i < numberNotes + 1; i++) {
            String title = preferences.getString(KEY_TITLE + i, KEY_TITLE);
            String description = preferences.getString(KEY_DESCRIPTION + i, KEY_DESCRIPTION);
            String priority = preferences.getString(KEY_PRIORITY + i, KEY_PRIORITY);

            if (!priority.equals(KEY_PRIORITY)) {
                notes.add(new LocaleNote(i, title, description, priority));
            }
        }
        return notes;
    }

    private LocaleNote changedNote(LocaleNote note) {
        if (databaseEditing(preferences, note)) {
            return note;
        }
        throw new RepositoryExeption("Change failed");
    }

    private LocaleNote addNewNote(LocaleNote note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(KEY_NUMBER_NOTES, counter);
        if (databaseEditing(editor, note, counter)) {
            note.setId(counter);
            counter++;
            return note;
        }
        throw new RepositoryExeption("Save failed");
    }

    private boolean databaseEditing(SharedPreferences preferences, LocaleNote note) {
        return databaseEditing(preferences.edit(), note, note.getId());
    }

    private boolean databaseEditing(SharedPreferences.Editor editor, LocaleNote note, long dbSize) {
        return preferences.edit()
                .putString(KEY_TITLE + dbSize, note.getTitle())
                .putString(KEY_DESCRIPTION + dbSize, note.getDescription())
                .putString(KEY_PRIORITY + dbSize, note.getPriority())
                .commit();
    }
}
