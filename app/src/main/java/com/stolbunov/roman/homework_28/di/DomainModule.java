package com.stolbunov.roman.homework_28.di;

import com.stolbunov.roman.data.repository.RepositoryManagerI;
import com.stolbunov.roman.data.repository.data_source.DBNoteRepository;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.domain.use_case.interactor.NoteListInteractor;
import com.stolbunov.roman.homework_28.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
class DomainModule {

    @AppScope
    @Provides
    INoteRepositoryBoundary provideBoundary(DBNoteRepository dbNoteRepository) {
        return new RepositoryManagerI(dbNoteRepository);
    }

    @AppScope
    @Provides
    IUseCase provideUseCase(INoteRepositoryBoundary boundary) {
        return new NoteListInteractor(boundary);
    }

}
