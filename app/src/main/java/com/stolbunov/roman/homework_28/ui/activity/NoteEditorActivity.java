package com.stolbunov.roman.homework_28.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_28.R;
import com.stolbunov.roman.homework_28.di.App;
import com.stolbunov.roman.homework_28.mvp.presenter.NoteEditorPresenter;
import com.stolbunov.roman.homework_28.mvp.view.NoteEditorView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.stolbunov.roman.homework_28.R.id.note_editor_fab;

public class NoteEditorActivity extends MvpAppCompatActivity implements NoteEditorView {
    private static final String KEY_INTENT_NOTE = "KEY_INTENT_NOTE";

    @Inject
    @InjectPresenter
    NoteEditorPresenter presenter;

    @ProvidePresenter
    NoteEditorPresenter provideNoteEditorPresenter() {
        return presenter;
    }

    @BindView(R.id.note_editor_title)
    AppCompatEditText title;
    @BindView(R.id.note_editor_description)
    AppCompatEditText description;
    @BindView(R.id.note_editor_show_priority)
    AppCompatTextView showPriority;
    @BindView(R.id.main_toolbar)
    Toolbar toolbar;

    public static Intent getIntent(Context context, Note note) {
        Intent intent = new Intent(context, NoteEditorActivity.class);
        intent.putExtra(KEY_INTENT_NOTE, note);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.get().injent(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);
        ButterKnife.bind(this);
        toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.note_editor);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Note note = intent.getParcelableExtra(KEY_INTENT_NOTE);

        filingFields(note);
    }

    private void filingFields(Note note) {
        title.setText(note.getTitle());
        description.setText(note.getDescription());
        showPriority.setText(note.getPriority().name());
    }

    @Override
    public void showPriority(int priority) {
        showPriority.setText(getString(priority));
    }

    @Override
    public void editNote(Note note) {
        Intent intent = MainActivity.getIntent(this, note);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick({R.id.note_editor_high_priority, R.id.note_editor_medium_priority,
            R.id.note_editor_low_priority, R.id.note_editor_fab})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.note_editor_high_priority:
                presenter.showSelectedPriority(R.string.high);
                break;
            case R.id.note_editor_medium_priority:
                presenter.showSelectedPriority(R.string.medium);
                break;
            case R.id.note_editor_low_priority:
                presenter.showSelectedPriority(R.string.low);
                break;
            case note_editor_fab:
                Note note = createNewNote();
                presenter.editNote(note);
                break;
        }
    }

    private Note createNewNote() {
        return new Note(title.getText().toString(),
                description.getText().toString(),
                Note.NotePriority.valueOf(showPriority.getText().toString()));
    }
}
