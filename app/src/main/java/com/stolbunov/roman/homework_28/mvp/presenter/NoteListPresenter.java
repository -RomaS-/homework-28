package com.stolbunov.roman.homework_28.mvp.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.homework_28.mvp.view.NoteListView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class NoteListPresenter extends MvpPresenter<NoteListView> {
    private final String ERROR_MESSAGE = "There was a problem loading the data. Try again.";
    private static String TAG = "TAG";

    @Inject
    IUseCase useCase;

    @Inject
    NoteListPresenter(IUseCase useCase) {
        this.useCase = useCase;
    }

    public void showDialog() {
        getViewState().showDialog();
    }

    public void hideDialog() {
        getViewState().hideDialog();
    }

    public void loadDataFromDB() {
        getViewState().showProgress();
        Disposable subscribe = useCase.getNoteList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::uploadSuccessful, err -> errorHandling(err, ERROR_MESSAGE));
    }

    public void add(Note note) {
        Disposable subscribe = useCase.add(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::operationSuccessful, this::errorHandling);
    }

    public void change(Note note) {
        Disposable subscribe = useCase.change(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getViewState()::change, this::errorHandling);
    }

    public void remove(Note note) {
        if (useCase.remove(note)) {
            getViewState().remove(note);
        } else {
            getViewState().showErrorSaveNoteMessage(ERROR_MESSAGE);
        }
    }

    private void errorHandling(Throwable throwable, String message) {
        Log.d(TAG, throwable.getMessage());
        getViewState().showErrorSaveNoteMessage(message);
    }

    private void errorHandling(Throwable throwable) {
        errorHandling(throwable, throwable.getMessage());
    }


    public void showErrorSaveMessage(String message) {
        getViewState().showErrorSaveNoteMessage(message);
    }

    public void editNote(Note note) {
        getViewState().goToEditor(note);
    }

    private void uploadSuccessful(List<Note> notes) {
        getViewState().hideProgress();
        getViewState().setDataAdapter(notes);
    }

    private void operationSuccessful(Note note) {
        getViewState().hideProgress();
        getViewState().add(note);
    }
}
