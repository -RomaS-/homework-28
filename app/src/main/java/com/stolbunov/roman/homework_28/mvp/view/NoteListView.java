package com.stolbunov.roman.homework_28.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.stolbunov.roman.domain.entity.Note;

import java.util.List;


public interface NoteListView extends MvpView {
    void add(Note note);

    @StateStrategyType(SkipStrategy.class)
    void remove(Note note);

    void change(Note note);

    void setDataAdapter(List<Note> notes);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showDialog();

    void hideDialog();

    void showErrorSaveNoteMessage(String message);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void goToEditor(Note note);

    void showProgress();

    void hideProgress();
}
