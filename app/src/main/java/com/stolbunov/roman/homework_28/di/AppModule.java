package com.stolbunov.roman.homework_28.di;

import android.content.Context;

import com.stolbunov.roman.homework_28.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule {
    private Context context;

    AppModule(Context context) {
        this.context = context;
    }

    @AppScope
    @Provides
    Context provideContext() {
        return context;
    }
}
