package com.stolbunov.roman.homework_28.ui.adapter;

import com.stolbunov.roman.domain.entity.Note;

public interface OnItemClickListener {
    void onItemClick(Note note);
}
