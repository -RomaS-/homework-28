package com.stolbunov.roman.homework_28.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_28.mvp.view.NoteEditorView;

import javax.inject.Inject;

@InjectViewState
public class NoteEditorPresenter extends MvpPresenter<NoteEditorView> {

    @Inject
    public NoteEditorPresenter() {
    }

    public void showSelectedPriority(int priority) {
        getViewState().showPriority(priority);
    }

    public void editNote(Note note) {
        getViewState().editNote(note);
    }
}
