package com.stolbunov.roman.homework_28.di;

import android.app.Application;

public class App extends Application {
    private static AppComponent component;


    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent get() {
        return component;
    }



}
