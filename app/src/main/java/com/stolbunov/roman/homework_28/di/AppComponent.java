package com.stolbunov.roman.homework_28.di;

import com.stolbunov.roman.homework_28.di.scope.AppScope;
import com.stolbunov.roman.homework_28.ui.activity.MainActivity;
import com.stolbunov.roman.homework_28.ui.activity.NoteEditorActivity;

import dagger.Component;

@AppScope
@Component(modules = {AppModule.class, RepositoryModule.class, DomainModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void injent(NoteEditorActivity noteEditorActivity);
}
