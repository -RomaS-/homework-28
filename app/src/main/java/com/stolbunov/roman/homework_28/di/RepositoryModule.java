package com.stolbunov.roman.homework_28.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.stolbunov.roman.data.repository.data_source.DBNoteRepository;
import com.stolbunov.roman.homework_28.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
class RepositoryModule {
    private final String NAME_DB = "notes";

    @AppScope
    @Provides
    SharedPreferences provideLocaleRepository(Context context) {
        return context.getSharedPreferences(NAME_DB, Context.MODE_PRIVATE);
    }

    @Provides
    DBNoteRepository provideDBNoteRepository(SharedPreferences sharedPreferences) {
        return new DBNoteRepository(sharedPreferences);
    }
}
