package com.stolbunov.roman.homework_28.ui.adapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_28.R;
import com.stolbunov.roman.homework_28.ui.widgets.CustomPriorityView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    private final int capacityList = 100;
    private List<Note> data;
    private OnRemoveNoteListener removeListener;
    private OnItemClickListener itemClickListener;

    @Inject
    public NoteAdapter() {
        data = new ArrayList<>(capacityList);
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_main_notes, viewGroup, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int position) {
        noteViewHolder.bind(getNoteByPosition(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Note> notes) {
        this.data.clear();
        this.data.addAll(notes);
        Collections.sort(this.data);
        notifyDataSetChanged();
    }

    public void add(Note note) {
        data.add(note);
        Collections.sort(data);
        notifyItemInserted(getNotePosition(note));

    }

    public void change(Note note) {
        add(note);
    }

    private Note getNoteByPosition(int position) {
        return data.get(position);
    }

    public void remove(Note note) {
        int position = getNotePosition(note);
        data.remove(note);
        notifyItemRemoved(position);
    }

    private void removeByPosition(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    private int getNotePosition(Note note) {
        return data.indexOf(note);
    }

    public void setRemoveListener(OnRemoveNoteListener removeListener) {
        this.removeListener = removeListener;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_note_title)
        AppCompatTextView title;
        @BindView(R.id.rv_note_description)
        AppCompatTextView description;
        @BindView(R.id.rv_note_remove)
        AppCompatImageButton remove;
        CustomPriorityView priority;


        NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            priority = itemView.findViewById(R.id.rv_note_priority);
        }

        void bind(Note note) {
            remove.setOnClickListener(v -> onRemoveClick(note));
            itemView.setOnClickListener(v -> onItemClick(note));

            title.setText(note.getTitle());
            description.setText(note.getDescription());
            fillingPriority(note);
        }

        private void fillingPriority(Note note) {
            switch (note.getPriority()) {
                case HIGH:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    priority.setCenterImageResource(R.drawable.ic_priority_high);
                    break;
                case MEDIUM:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.YELLOW));
                    priority.setCenterImageResource(R.drawable.ic_priority_medium);
                    break;
                case LOW:
                    priority.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    priority.setCenterImageResource(R.drawable.ic_priority_low);
                    break;
            }
        }

        private void onItemClick(Note note) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(note);
                remove(note);
            }
        }

        private void onRemoveClick(Note note) {
            if (removeListener != null) {
                removeListener.onRemoveNote(note);
            }
        }
    }

    public interface OnRemoveNoteListener {
        void onRemoveNote(Note note);
    }
}