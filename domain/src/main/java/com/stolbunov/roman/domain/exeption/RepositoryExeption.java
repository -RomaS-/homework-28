package com.stolbunov.roman.domain.exeption;

public class RepositoryExeption extends RuntimeException {
    public RepositoryExeption() {
        super();
    }

    public RepositoryExeption(String message) {
        super(message);
    }
}
