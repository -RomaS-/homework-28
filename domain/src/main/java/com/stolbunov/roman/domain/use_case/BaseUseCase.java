package com.stolbunov.roman.domain.use_case;

import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;

import javax.inject.Inject;

public abstract class BaseUseCase implements IUseCase {

    @Inject
    protected INoteRepositoryBoundary boundary;

    public BaseUseCase(INoteRepositoryBoundary boundary) {
        this.boundary = boundary;
    }
}
